/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package cat.copernic.lambdaexemples.Interficies;

/**
 *
 * @author EstherSanchez
 */
@FunctionalInterface //anotació opcional que evita errors
public interface Parametre {
        public abstract double getValor(double valor); 
        //només es declara el mètode. 
        //El mètode getValor s’implementa en el moment que es crea l’expressió lambda

}


