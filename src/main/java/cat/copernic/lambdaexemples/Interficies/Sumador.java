/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package cat.copernic.lambdaexemples.Interficies;

/**
 *
 * @author EstherSanchez
 */
@FunctionalInterface
public interface Sumador {
    public abstract int suma(int a, int b); 

}
