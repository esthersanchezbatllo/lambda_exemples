/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package cat.copernic.lambdaexemples;

import cat.copernic.lambdaexemples.Interficies.Sumador;
import cat.copernic.lambdaexemples.Interficies.Parametre;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 *
 * @author EstherSanchez
 */
public class LambdaExemples {
 private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static void main(String[] args) {
       
        /* Primer exemple */
        Parametre p1 = (n) -> 1.0 / n;    //Parametre és la FunctionalInterface
        //(n) --> paràmetre
        // -> implementació de la lambda on fa el càlcul de l'invers pel mètode getValor 
        System.out.println("L'invers de 4 és " + p1.getValor(4.0));
        
        /*Segon exemple */
        Sumador sumar = (a, b) -> a + b; //implementació del mètode suma
        // Cridem a la función lambda 
        int resultat = sumar.suma(5, 3); // resultat serà igual a 8 
        System.out.println(resultat); 
        
        /* Tercer exemple de Supplier<T>*/
        Supplier<LocalDateTime> s = () -> LocalDateTime.now();
        LocalDateTime time = s.get();
        System.out.println(time);
        Supplier<String> s1 = () -> dtf.format(LocalDateTime.now());
        String time2 = s1.get();
        System.out.println(time2);

        /* Quart exemple de Function<T,R>*/
        Function<String, Integer> func = (x) -> x.length(); 
        Integer apply = func.apply("mkyong"); // 6 
        System.out.println(apply); 

        /* Cinquè exemple de Function<T,R>*/
        Function<Double, Double> duplicar = x -> x * 2; //duplicar
        double resultatd=duplicar.apply(2.0);
        System.out.println(resultatd);
        Function<Double,Double> sumar2= x->x+2; //sumar +2
        double resultat2=sumar2.apply(resultatd);
        System.out.println(resultat2);
        
        /* Sisè exemple combinar funció amb andThen */
        Function<Double, Double> composicio = duplicar.andThen(sumar2);
        resultat2=composicio.apply(2.0);
        System.out.println(resultat2);


    }
}
